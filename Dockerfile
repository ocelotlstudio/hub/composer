# syntax=docker/dockerfile:1
FROM php:8.1-bullseye
SHELL ["/bin/bash", "--login", "-c"]
RUN apt update && apt upgrade -y
RUN apt install curl git zip libzip-dev libicu-dev libxml2-dev libxslt-dev -y
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN docker-php-ext-install zip && docker-php-ext-enable zip
RUN docker-php-ext-install intl && docker-php-ext-enable intl
RUN docker-php-ext-install soap && docker-php-ext-enable soap
RUN docker-php-ext-install xsl && docker-php-ext-enable xsl

